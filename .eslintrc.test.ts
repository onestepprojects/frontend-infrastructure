import { ESLint } from 'eslint'
import baseConfig from './.eslintrc'

describe('eslint', () => {
  describe('config', () => {
    const eslint = new ESLint({ baseConfig, useEslintrc: false })

    it('exports eslint config', async () => {
      await expect(eslint.calculateConfigForFile(__filename)).resolves.toMatchObject({
        env: {
          browser: true,
          jest: true,
          node: true,
        },
      })
    })
  })

  describe('typescript-eslint', () => {
    const eslint = new ESLint({ baseConfig })

    it('excludes typing rules', async () => {
      const results = await eslint.lintText(
        /**
         * `@typescript-eslint/await-thenable` is a recommended requires type rule. Ensure exclude it.
         *
         * @see https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/await-thenable.md
         */
        `(async () => await 1)()`
      )

      expect(results[0].errorCount).toBe(0)
    })
  })
})
