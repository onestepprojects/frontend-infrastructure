require('@testing-library/jest-dom')

process.env.TZ = 'UTC'

/**
 * Avoid async-validator global warning.
 *
 * @see https://github.com/yiminghe/async-validator#faq
 */
globalThis.ASYNC_VALIDATOR_NO_WARNING = 1

/** Antd depend on getComputedStyle method. */
Object.defineProperty(globalThis, 'getComputedStyle', {
  writable: true,
  value: function getComputedStyle() {
    return {
      width: '1px',
      height: '1px',
      /** Testing-library depend on getPropertyValue method if getComputedStyle is defined. */
      getPropertyValue() {},
    }
  },
})

/** Antd depend on matchMedia method. */
Object.defineProperty(globalThis, 'matchMedia', {
  writable: true,
  value: function matchMedia() {
    return {
      addListener() {},
      removeListener() {},
    }
  },
})

Object.defineProperty(globalThis, 'fetch', {
  writable: true,
  value: function fetch() {
    return {
      clone() {
        return this
      },
    }
  },
})

Object.defineProperty(globalThis, 'Request', {
  writable: true,
  value: function Request() {
    return {
      clone() {
        return this
      },
    }
  },
})
