/** @type {import('./types').PrettierConfig} */
module.exports = {
  printWidth: 100,
  semi: false,
  singleQuote: true,
  jsxSingleQuote: true,
}
