import type { UserConfig, UserConfigExport } from 'vite'

type OneStepUserConfig = UserConfig & {
  /**
   * Base public path when served in development or production. Valid values include:
   *
   * - Absolute URL pathname, e.g. `/foo/`
   * - Full URL, e.g. `https://foo.com/`
   * - Empty string or `./` (for embedded deployment)
   *
   * @default '/'
   * @see https://vitejs.dev/config/#base
   */
  base?: string

  /**
   * A utility-first CSS framework.
   *
   * @default false
   * @see https://tailwindcss.com/
   */
  tailwindcss?: boolean
}
type OneStepUserConfigFn = (env: ConfigEnv) => OneStepUserConfig | Promise<OneStepUserConfig>
type OneStepUserConfigExport = OneStepUserConfig | Promise<OneStepUserConfig> | OneStepUserConfigFn

export const defineConfig: (config?: OneStepUserConfigExport) => UserConfigExport
