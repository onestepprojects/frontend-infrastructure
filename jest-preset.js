const path = require('path')

const resolve = (p) => path.resolve(__dirname, p)

const jestTransformer = resolve('jest-transformer.js')

/** @type {import('./types').JestConfig} */
module.exports = {
  logHeapUsage: true,

  moduleNameMapper: {
    '\\.module\\.(css|pcss|postcss|less|sass|scss|styl)$': 'identity-obj-proxy',
    '\\.(css|pcss|postcss|less|sass|scss|styl)$': jestTransformer,
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': [
      jestTransformer,
      { type: 'filename' },
    ],
  },

  passWithNoTests: true,

  setupFilesAfterEnv: [resolve('jest-setup.js')],

  testEnvironment: 'jsdom',

  transform: {
    '^.+\\.m?[tj]sx?$': [
      require.resolve('ts-jest'),
      {
        diagnostics: false,
        isolatedModules: true,
      },
    ],
  },
}
