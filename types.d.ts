import type * as eslint from 'eslint'
import type * as postcss from 'postcss'

export type { Config as PrettierConfig } from 'prettier'
export type { Options as ReleaseConfig } from 'semantic-release'
export type { Config as TailwindConfig } from 'tailwindcss'
export type { JestConfigWithTsJest as JestConfig } from 'ts-jest'

export type ESLintConfig = eslint.Linter.Config
export type PostCSSConfig = postcss.ProcessOptions & { plugins?: postcss.Plugin[] }
