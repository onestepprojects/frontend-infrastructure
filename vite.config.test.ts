/** @jest-environment node */
import type { UserConfig, UserConfigFn } from 'vite'
import pkg from './package.json'
import { defineConfig } from './vite.config'

describe('vite', () => {
  describe('config', () => {
    const configFn = defineConfig() as UserConfigFn

    it('exports vite create config function', () => {
      expect(configFn).toBeInstanceOf(Function)
    })

    it('creates vite config', async () => {
      const config = await configFn({ mode: 'test', command: 'build' })

      expect(config).toMatchObject<UserConfig>({
        build: {
          lib: {
            entry: expect.stringContaining('index.ts'),
          },
          minify: false,
          sourcemap: true,
        },

        esbuild: {
          loader: 'tsx',
        },

        server: {
          strictPort: true,
        },
      })
    })

    it('externals dependencies', async () => {
      const config = await configFn({ mode: 'test', command: 'build' })

      const external = config.build.rollupOptions.external as (source: string) => boolean

      expect(external(Object.keys(pkg.dependencies)[0])).toBe(true)

      expect(external(Object.keys(pkg.devDependencies)[0])).toBe(false)

      expect(external('./utils')).toBe(false)
      expect(external('/utils')).toBe(false)
    })
  })
})
