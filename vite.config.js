const fs = require('fs')
const path = require('path')
const findUp = require('find-up')
const killPort = require('kill-port')
const colors = require('picocolors')
const { createLogger, loadEnv, mergeConfig } = require('vite')
const { esbuildCommonjs } = require('@originjs/vite-plugin-commonjs')
const react = require('@vitejs/plugin-react')

const getPkg = () => ({
  source: './src/index.ts',
  main: './dist/index.cjs',
  module: './dist/index.js',
  style: './dist/index.css',
  types: './src/index.ts',
  ...require(findUp.sync('package.json')),
})

const logger = createLogger('info', { allowClearScreen: false })

/**
 * Pick env vars which defined for react-scripts.
 *
 * @param {import('vite').ConfigEnv} env
 */
const getProcessEnv = ({ mode, command }) => {
  if (command === 'build') {
    // Leave it as is and pass it to webpack.
    return { 'process.env': 'process.env' }
  }

  const reactAppEnv = loadEnv(mode, '', ['REACT_APP_'])

  if (Object.keys(reactAppEnv).length) {
    return Object.fromEntries(
      Object.entries(reactAppEnv).map(([key, value]) => [
        `process.env.${key}`,
        JSON.stringify(value),
      ])
    )
  } else {
    const coloredEnvFileName = colors.yellow(colors.bold(`.env.${mode}.local`))
    logger.warn(`Please add ${coloredEnvFileName} to the root of project.`, {
      timestamp: true,
    })
  }
}

/**
 * Get valid localhost dev cert.
 *
 * @param {import('vite').ConfigEnv} env
 */
const getHttps = async ({ command }) => {
  if (command === 'serve') {
    const devcert = await import('devcert')
    return await devcert.certificateFor('localhost')
  }
}

/** Generate `tailwind.config.js` and `postcss.config.js` if needed. */
const ensureTailwindConfig = () => {
  const tailwindConfigFilename = 'tailwind.config.js'
  const postcssConfigFilename = 'postcss.config.js'

  if (!fs.existsSync(tailwindConfigFilename)) {
    const coloredConfigFileName = colors.cyan(colors.bold(tailwindConfigFilename))
    logger.info(`Generate ./${coloredConfigFileName}`, {
      timestamp: true,
    })
    const { name } = require('./package.json')
    fs.writeFileSync(
      tailwindConfigFilename,
      `/** @type {import('${name}').TailwindConfig} */
module.exports = { presets: [require('${name}/tailwind.config')] }`
    )
  }

  if (!fs.existsSync(postcssConfigFilename)) {
    const coloredConfigFileName = colors.cyan(colors.bold(postcssConfigFilename))
    logger.info(`Generate ./${coloredConfigFileName}`, {
      timestamp: true,
    })
    fs.writeFileSync(
      postcssConfigFilename,
      `module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss/nesting'),
    require('tailwindcss'),
    require('autoprefixer'),
  ],
}`
    )
  }
}

/**
 * @param {import('./vite.config').OneStepUserConfigExport} config
 * @returns {import('vite').UserConfigExport}
 */
exports.defineConfig = (config = {}) => {
  return async ({ mode, command }) => {
    const {
      base = '',
      tailwindcss,
      ...userConfig
    } = await (typeof config === 'function' ? config({ mode, command }) : config || {})

    if (command === 'serve') {
      await killPort(3000)
    }

    const pkg = getPkg()

    const externalDependencyNames = Object.keys({ ...pkg.dependencies, ...pkg.peerDependencies })

    if (tailwindcss) {
      ensureTailwindConfig()
    }

    /** @type {import('vite').UserConfig} */
    const defaultConfig = {
      clearScreen: false,

      base,

      define: {
        ...getProcessEnv({ mode, command }),
      },

      envPrefix: 'REACT_APP_',

      resolve: {
        alias: [
          /**
           * Fix amplify build
           *
           * @see https://ui.docs.amplify.aws/getting-started/installation?platform=vue
           */
          {
            find: './runtimeConfig',
            replacement: './runtimeConfig.browser',
          },
        ],
      },

      plugins: [react()],

      build: {
        lib: {
          entry: path.resolve(pkg.source),
          formats: ['cjs', 'es'],
          fileName: (format) => path.basename({ cjs: pkg.main, es: pkg.module }[format]),
        },
        emptyOutDir: !process.argv.includes('--watch'),
        minify: false,
        sourcemap: true,
        rollupOptions: {
          external: (id) =>
            !id.startsWith('.') &&
            !path.isAbsolute(id) &&
            externalDependencyNames.some((name) => id.startsWith(name)),
          output: {
            assetFileNames: ({ name }) => (name === 'style.css' ? path.basename(pkg.style) : name),
          },
        },
      },

      esbuild: {
        loader: 'tsx',
        include: /src\/.+\.(j|t)sx?$/,
        exclude: [],
      },
      optimizeDeps: {
        esbuildOptions: {
          plugins: [
            esbuildCommonjs(['react-calendar', 'react-date-picker']),
            /**
             * Plugin to force trust `.js` as `.jsx`.
             *
             * @see https://github.com/vitejs/vite/discussions/3448#discussioncomment-749919
             */
            {
              name: 'load-js-files-as-jsx',
              setup(build) {
                build.onLoad({ filter: /src\/.+\.js$/ }, async (args) => ({
                  loader: 'tsx',
                  contents: fs.readFileSync(args.path, 'utf8'),
                }))
              },
            },
          ],
        },
      },

      server: {
        https: await getHttps({ command }),
        strictPort: true,
      },
    }

    return mergeConfig(defaultConfig, userConfig)
  }
}
