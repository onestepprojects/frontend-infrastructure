const typescriptRules = require('@typescript-eslint/eslint-plugin').rules
const react = (() => {
  try {
    return require('react')
  } catch {}
})()

/**
 * @type {{ rules: Record<string, import('@typescript-eslint/utils').TSESLint.RuleModule<string>>; prefix: string; }[]}
 */
const rulePrefixDefinitions = [
  { rules: Object.fromEntries(require('eslint/use-at-your-own-risk').builtinRules), prefix: '' },

  { rules: typescriptRules, prefix: '@typescript-eslint' },
  { rules: require('eslint-plugin-import').rules, prefix: 'import' },
  { rules: require('eslint-plugin-n').rules, prefix: 'n' },
  { rules: require('eslint-plugin-promise').rules, prefix: 'promise' },

  ...(react
    ? [
        { rules: require('eslint-plugin-react').rules, prefix: 'react' },
        { rules: require('eslint-plugin-react-hooks').rules, prefix: 'react-hooks' },
      ]
    : []),
]

const ruleRecords = rulePrefixDefinitions.flatMap(({ rules, prefix }) =>
  Object.entries(rules).map(([name, rule]) => ({
    name: [prefix, name].filter(Boolean).join('/'),
    rule,
  }))
)

/** @type {import('./types').ESLintConfig} */
module.exports = {
  extends: [
    'standard-with-typescript',

    ...(react
      ? ['standard-react', 'plugin:react/jsx-runtime', 'plugin:react-hooks/recommended']
      : []),

    'prettier',
  ],

  env: {
    browser: true,
    jest: true,
  },

  rules: {},

  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        /**
         * Require using either `T[]` or `Array<T>` for arrays.
         *
         * @default ['error', { default: 'array-simple' }]
         * @see https://typescript-eslint.io/rules/array-type
         */
        '@typescript-eslint/array-type': 'off',

        /**
         * Enforce consistent usage of type assertions.
         *
         * @default 'warn'
         * @see https://typescript-eslint.io/rules/consistent-type-assertions
         */
        '@typescript-eslint/consistent-type-assertions': 'off',

        /**
         * Require explicit return types on functions and class methods.
         *
         * @default 'warn'
         * @see https://typescript-eslint.io/rules/explicit-function-return-type
         */
        '@typescript-eslint/explicit-function-return-type': 'off',

        /**
         * Disallow `void` type outside of generic or return types.
         *
         * @default 'warn'
         * @see https://typescript-eslint.io/rules/no-invalid-void-type
         */
        '@typescript-eslint/no-invalid-void-type': 'off',

        /**
         * Disallow unused variables.
         *
         * @default 'warn'
         * @see https://typescript-eslint.io/rules/no-unused-vars
         */
        '@typescript-eslint/no-unused-vars': 'off',

        /**
         * Disable typescript-eslint rules with type information for performance.
         *
         * @see https://typescript-eslint.io/docs/linting/type-linting/
         */
        ...ruleRecords
          .filter(({ rule }) => rule.meta.docs?.requiresTypeChecking)
          .reduce((rules, { name }) => ({ ...rules, [name]: 'off' }), {}),
      },
    },
  ],
}
