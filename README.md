# OneStep Frontend Infrastructure

## Getting started

`npm i -D @onestepprojects/frontend-infrastructure`

### Bundled dev tools

- eslint
- husky@4
- jest
- lint-staged
- postcss
- prettier
- semantic-release
- tailwindcss
- typescript
- vite

### Example

`package.json`

```json
{
  "name": "@onestepprojects/module-name",
  "version": "1.0.0",
  "description": "A description of my module",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/onestepprojects/module-name.git"
  },
  "license": "AGPL_3.0-only",
  "sideEffects": ["*.css"],
  "main": "dist/index.js",
  "module": "dist/index.es.js",
  "source": "src/index.ts",
  "types": "src/index.ts",
  "style": "dist/index.css",
  "files": ["dist", "src"],
  "scripts": {
    "dev": "vite",
    "start": "vite build --watch",
    "build": "vite build",
    "lint": "eslint 'src/**/*.{js,jsx,ts,tsx}'",
    "test": "jest",
    "semantic-release": "semantic-release"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*": "prettier --write --ignore-unknown"
  },
  "prettier": "@onestepprojects/frontend-infrastructure/prettier.config",
  "eslintConfig": {
    "extends": "./node_modules/@onestepprojects/frontend-infrastructure/.eslintrc"
  },
  "release": {
    "extends": "@onestepprojects/frontend-infrastructure/release.config"
  },
  "jest": {
    "preset": "@onestepprojects/frontend-infrastructure"
  }
}
```

## Shared Configs

To make it easy for you to create a new frontend project, here's a list of recommended default configs.

### ESLint

https://eslint.org/docs/developer-guide/shareable-configs

Note the `./node_modules/` prefix. Because our package name does not match the eslint package name `eslint-config-*` rule.

`.eslintrc.js`

```js
/** @type {import('@onestepprojects/frontend-infrastructure').ESLintConfig} */
module.exports = {
  extends: './node_modules/@onestepprojects/frontend-infrastructure/.eslintrc',
}
```

or `eslintConfig` field in package.json

```json
{
  "eslintConfig": {
    "extends": "./node_modules/@onestepprojects/frontend-infrastructure/.eslintrc"
  }
}
```

### Jest

https://jestjs.io/docs/configuration#preset-string

Note we don't need to write the `/jest-preset` filename. Jest always reads `jest-preset.js`.

`jest.config.js`

```js
/** @type {import('@onestepprojects/frontend-infrastructure').JestConfig} */
module.exports = {
  preset: '@onestepprojects/frontend-infrastructure',
}
```

or `jest` field in package.json

```json
{
  "jest": {
    "preset": ["@onestepprojects/frontend-infrastructure"]
  }
}
```

### Prettier

https://prettier.io/docs/en/configuration.html#sharing-configurations

`.prettierrc.js`

```js
module.exports = '@onestepprojects/frontend-infrastructure/prettier.config'
```

or `prettier` field in package.json

```json
{
  "prettier": "@onestepprojects/frontend-infrastructure/prettier.config"
}
```

### Semantic release

https://semantic-release.gitbook.io/semantic-release/usage/shareable-configurations

`release.config.js`

```js
/** @type {import('@onestepprojects/frontend-infrastructure').ReleaseConfig} */
module.exports = {
  extends: '@onestepprojects/frontend-infrastructure/release.config',
}
```

or `release` field in package.json

```json
{
  "release": {
    "extends": "@onestepprojects/frontend-infrastructure/release.config"
  }
}
```

### TailwindCSS

https://tailwindcss.com/docs/presets#creating-a-preset

`tailwind.config.js`

```js
/** @type {import('@onestepprojects/frontend-infrastructure').TailwindConfig} */
module.exports = {
  presets: [require('@onestepprojects/frontend-infrastructure/tailwind.config')],
}
```

### TypeScript

https://www.typescriptlang.org/tsconfig#extends

`tsconfig.json`

```json
{
  "extends": "@onestepprojects/frontend-infrastructure/tsconfig.json",
  "include": ["./src"]
}
```

### Vite

`vite.config.ts`

```ts
import { defineConfig } from '@onestepprojects/frontend-infrastructure/vite.config'

export default defineConfig({ base: '/', tailwindcss: false })
```

Vite external dependencies and peerDependencies by rollup in build mode. Since the dependencies should be install in the parent app. The devDependencies and source code will be bundled to dist.

The default value of the tailwindcss option is `false`. It is activated when set to `true`. This will creating `tailwind.config.js` and `postcss.config.js` automatically if not exist.

Vite does provide built-in support for `sass`, `less` and `stylus` files. Just run `npm i -D sass` to install the corresponding preprocessors.

## Development

See `files` field in package.json. These files will be published.

If you want to test this package locally. ~~`npm link`~~ is not works. You should use `npm pack` to create a tarball. Then install it by `npm i ../../frontend-infrastructure-x.x.x.tgz` under the root of your project.

### Commands

- `npm run lint` - Run eslint to check code style by `.eslintrc.js`. Help you verify the eslint rules before publish.
- `npm run semantic-release` - Run semantic-release to publish the package on CI.
- `npm run test` - Run jest to test the code by `jest-preset.js`. Help you verify the jest config before publish.
- `npm run test:watch` - Run jest in watch mode.
